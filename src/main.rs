use quote::ToTokens;
use std::io::Write;

fn main() -> Result<(), anyhow::Error> {
    let app = clap::App::new("obelix")
        .about("A tool to reduce Rust source files to produce MVEs.")
        .version(clap::crate_version!())
        .arg(
            clap::Arg::with_name("INPUT")
                .help("Sets the input file to use")
                .index(1),
        )
        .get_matches();

    let input = app
        .value_of_os("INPUT")
        .unwrap_or(std::ffi::OsStr::new("-"));

    let mut input_stream_file;
    let stdin;
    let mut stdin_lock;
    let input_stream: &mut dyn std::io::Read;
    if input != "-" {
        input_stream_file = std::fs::File::open(input)?;
        input_stream = &mut input_stream_file;
    } else {
        stdin = std::io::stdin();
        stdin_lock = stdin.lock();
        input_stream = &mut stdin_lock;
    }

    let mut buffer = String::new();
    input_stream.read_to_string(&mut buffer)?;

    let compiler = obelix::Compiler::default();

    let mut file = syn::parse_str::<syn::File>(&buffer)?;
    let mut registry = obelix::FoldRegistry::new();
    registry.simplify_file(&compiler, &mut file);

    let mut stdout = std::io::stdout();
    stdout.write(file.into_token_stream().to_string().as_bytes())?;

    Ok(())
}
