#[derive(Copy, Clone, Debug, serde::Deserialize, PartialEq)]
enum SuggestionApplicability {
    MachineApplicable,
    MaybeIncorrect,
}

#[derive(Clone, Debug, serde::Deserialize, derivative::Derivative)]
#[derivative(PartialEq)]
struct Span {
    label: Option<String>,
    suggestion_applicability: Option<SuggestionApplicability>,
    suggested_replacement: Option<String>,
    #[derivative(PartialEq = "ignore")]
    byte_start: usize,
    #[derivative(PartialEq = "ignore")]
    byte_end: usize,
    #[derivative(PartialEq = "ignore")]
    line_start: usize,
    #[derivative(PartialEq = "ignore")]
    line_end: usize,
    #[derivative(PartialEq = "ignore")]
    column_start: usize,
    #[derivative(PartialEq = "ignore")]
    column_end: usize,
}

impl Span {
    pub fn is_machine_applicable(&self) -> bool {
        self.suggestion_applicability == Some(SuggestionApplicability::MachineApplicable)
    }

    fn to_line_column_span(&self) -> crate::span::LineColumnSpan {
        crate::span::LineColumnSpan {
            start: crate::span::Cell::new(self.line_start, self.column_start),
            end: crate::span::Cell::new(self.line_end, self.column_end),
        }
    }
}

#[derive(Clone, Debug, serde::Deserialize, PartialEq)]
struct Code {
    code: String,
}

#[derive(Debug, serde::Deserialize, PartialEq)]
#[serde(rename_all = "kebab-case")]
enum Level {
    /// A fatal error that prevents compilation.
    Error,
    /// A possible error or concern.
    Warning,
    /// Additional information or context about the diagnostic.
    Note,
    /// A suggestion on how to resolve the diagnostic.
    Help,
    /// A note attached to the message for further information.
    FailureNote,
    /// Indicates a bug within the compiler.
    #[serde(rename = "error: internal compiler error")]
    Ice,
    /// A message that wasn't outputted at JSON. Encountered during ICEs.
    None,
}

#[derive(Debug, serde::Deserialize, PartialEq)]
pub struct Message {
    message: String,
    code: Option<Code>,
    level: Level,
    spans: Vec<Span>,
    children: Vec<Message>,
}

impl Message {
    const DEAD_CODE_IDS: &'static [&'static str] = &[
        "dead_code",
        "unused_braces",
        "unused_imports",
        "unused_mut",
        "unused_variables",
        "unused_parens",
    ];

    pub(crate) fn is_dead_code(&self) -> bool {
        self.code
            .as_ref()
            .map(|m| m.code.as_ref())
            .map(|c| Message::DEAD_CODE_IDS.contains(&c))
            .unwrap_or(false)
    }

    pub(crate) fn ignorable_for_equality(&self) -> bool {
        self.message.starts_with("aborting due to previous error")
    }
}

/// List of messages that include span information that should be ignored when checking
/// whether the messages are equivalent.
static MESSAGES_RE_RAW: &[&str] = &[
    r"first, the lifetime cannot outlive the lifetime `'.*` as defined on the function body at (?P<R>.+)...",
    r"first, the lifetime cannot outlive the anonymous lifetime #\d* defined on the method body at (?P<R>.+)...",
    r"...so that the type `\[(?P<R>.+)\]` will meet its required lifetime bounds",
    // produced during ICEs:
    r"(?P<R><anon>:\d+:\d+: \d+:\d+ \(#\d+\))",
];

static MESSAGES_REGEX_SET: once_cell::sync::Lazy<regex::RegexSet> =
    once_cell::sync::Lazy::new(|| regex::RegexSet::new(MESSAGES_RE_RAW).unwrap());

static MESSAGES_REGEXES: once_cell::sync::Lazy<Vec<regex::Regex>> =
    once_cell::sync::Lazy::new(|| {
        MESSAGES_RE_RAW
            .iter()
            .map(|r| regex::Regex::new(r).unwrap())
            .collect()
    });

impl Message {
    pub fn from_compiler_output(output: &[u8]) -> Vec<Message> {
        output
            .split(|&b| b == b'\n')
            // filter empty lines out
            .filter(|s| !s.is_empty())
            .map(|line| match serde_json::from_slice::<Message>(line) {
                Ok(mut m) => {
                    m.cleanup();
                    m
                }
                Err(_) => Message::new_without_level(line),
            })
            .collect::<Vec<_>>()
    }

    /// Cleanup all "unused code" spans and returns whether any change has
    /// been made that would invalidate cached spans.
    #[cfg_attr(
        feature = "debug",
        tracing_attributes::instrument(fields(output = "123", invalidate_spans = true))
    )]
    pub fn cleanup_unused(messages: &[Message], content: &mut String) -> bool {
        let mut applicable_spans = messages
            .iter()
            .flat_map(|m| m.children.iter().chain(std::iter::once(m)))
            .filter(|m| m.is_dead_code())
            .flat_map(|m| m.spans.iter())
            .filter(|s| s.is_machine_applicable())
            .collect::<Vec<_>>();

        // sort in reverse order to prevent changes to affect the next ones
        applicable_spans.sort_by(|a, b| b.byte_start.cmp(&a.byte_start));

        // make sure than no spans overlap
        for ms in applicable_spans.windows(2) {
            if let [a, b] = ms {
                if a.byte_start > b.byte_end {
                    return false;
                }
            }
        }

        let mut invalidate_spans = false;

        let mut content_bytes = std::mem::replace(content, String::new()).into_bytes();
        for span in applicable_spans {
            if let Some(replacement) = &span.suggested_replacement {
                invalidate_spans =
                    invalidate_spans || (span.byte_end - span.byte_start) != replacement.len();
                content_bytes.splice(
                    span.byte_start..span.byte_end,
                    replacement.as_bytes().iter().cloned(),
                );
            }
        }

        *content = String::from_utf8(content_bytes).expect("Invalid simplified code");

        #[cfg(feature = "debug")]
        {
            tracing::Span::current().record("output", &content.as_str());
            tracing::Span::current().record("invalidate_spans", &invalidate_spans);
        }

        invalidate_spans
    }

    fn new_without_level(line: &[u8]) -> Message {
        Message {
            message: String::from_utf8_lossy(line).into_owned(),
            code: None,
            level: Level::None,
            spans: Vec::new(),
            children: Vec::new(),
        }
    }

    fn cleanup(&mut self) {
        for i in MESSAGES_REGEX_SET.matches(&self.message) {
            let re = &MESSAGES_REGEXES[i];

            while let Some(range) = re
                .captures(&self.message)
                .and_then(|captures| captures.name("R").map(|m| m.range()))
            {
                self.message.replace_range(range, "");
            }
        }

        for m in &mut self.children {
            // Some messages don't carry their parent's code,
            // but we use the code to know whether the suggestion is useful for Obelix
            if m.code.is_none() {
                m.code = self.code.clone();
            }

            m.cleanup();
        }
    }

    pub fn spans<'a>(&'a self) -> impl Iterator<Item = crate::span::LineColumnSpan> + 'a {
        self.spans.iter().map(Span::to_line_column_span)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn cleanup_messages() {
        let cases = &[
            r"first, the lifetime cannot outlive the lifetime `'1` as defined on the function body at 1:2...",
            r"first, the lifetime cannot outlive the lifetime `'1` as defined on the function body at ...",
            r"first, the lifetime cannot outlive the anonymous lifetime #1 defined on the method body at 1:2...",
            r"first, the lifetime cannot outlive the anonymous lifetime #1 defined on the method body at ...",
            r"...so that the type `[Foo<'a>]` will meet its required lifetime bounds",
            r"...so that the type `[]` will meet its required lifetime bounds",
            r"span: <anon>:1:835: 1:838 (#0)",
            r"span: ",
            r"span: <anon>:1:835: 1:838 (#0) … span: <anon>:2:836: 2:839 (#1)",
            r"span:  … span: ",
        ];

        for case in cases.chunks(2) {
            let mut message = super::Message::new_without_level(case[0].as_bytes());
            message.cleanup();
            assert_eq!(message.message, case[1]);
        }
    }
}
