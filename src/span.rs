#[derive(Ord, PartialOrd, Eq, PartialEq, Debug)]
pub struct LineColumnSpan {
    pub start: Cell,
    pub end: Cell,
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub struct Cell {
    pub line: usize,
    pub column: usize,
}

impl std::fmt::Debug for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", self.line, self.column)
    }
}

impl Cell {
    pub fn new(line: usize, column: usize) -> Self {
        Self { line, column }
    }
}

impl LineColumnSpan {
    pub fn intersects(&self, other: &LineColumnSpan) -> bool {
        self.start <= other.start && other.start <= self.end
            || self.start <= other.end && other.end <= self.end
            || other.start <= self.start && self.start <= other.end
            || other.start <= self.end && self.end <= other.end
    }
}

impl From<proc_macro2::Span> for LineColumnSpan {
    fn from(other: proc_macro2::Span) -> Self {
        Self {
            start: Cell::new(other.start().line, other.start().column),
            end: Cell::new(other.end().line, other.end().column),
        }
    }
}
