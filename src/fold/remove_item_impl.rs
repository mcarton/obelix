use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveItemImplVisitor(State);

impl FolderImpl for RemoveItemImplVisitor {
    const NAME: &'static str = "RemoveItemImpl";

    fn new(state: super::common_folder::State) -> Self {
        RemoveItemImplVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

trait Items {
    fn len(&self) -> usize;

    fn span(&self, index: usize) -> proc_macro2::Span;
    fn remove(&mut self, index: usize);

    fn fold(self, visitor: &mut RemoveItemImplVisitor) -> Self;
}

impl Items for syn::File {
    fn len(&self) -> usize {
        self.items.len()
    }

    fn span(&self, index: usize) -> proc_macro2::Span {
        self.items[index].span()
    }

    fn remove(&mut self, index: usize) {
        self.items.remove(index);
    }

    fn fold(self, visitor: &mut RemoveItemImplVisitor) -> Self {
        syn::fold::fold_file(visitor, self)
    }
}

impl Items for syn::ItemImpl {
    fn len(&self) -> usize {
        self.items.len()
    }

    fn span(&self, index: usize) -> proc_macro2::Span {
        self.items[index].span()
    }

    fn remove(&mut self, index: usize) {
        self.items.remove(index);
    }

    fn fold(self, visitor: &mut RemoveItemImplVisitor) -> Self {
        syn::fold::fold_item_impl(visitor, self)
    }
}

impl Items for syn::ItemTrait {
    fn len(&self) -> usize {
        self.items.len()
    }

    fn span(&self, index: usize) -> proc_macro2::Span {
        self.items[index].span()
    }

    fn remove(&mut self, index: usize) {
        self.items.remove(index);
    }

    fn fold(self, visitor: &mut RemoveItemImplVisitor) -> Self {
        syn::fold::fold_item_trait(visitor, self)
    }
}

fn fold_file<T: Items>(mut items: T, visitor: &mut RemoveItemImplVisitor) -> T {
    if visitor.0.done {
        return items;
    }

    let mut i = items.len();

    while i != 0 {
        i = i.saturating_sub(1);

        let item = items.span(i);

        if visitor.0.visited.insert(item.into())
            && !visitor
                .0
                .expected_spans
                .iter()
                .any(|s| s.intersects(&item.into()))
        {
            items.remove(i);
            visitor.0.done = true;
            return items;
        }
    }

    items.fold(visitor)
}

impl syn::fold::Fold for RemoveItemImplVisitor {
    fn fold_file(&mut self, file: syn::File) -> syn::File {
        fold_file(file, self)
    }

    fn fold_item_impl(&mut self, r#impl: syn::ItemImpl) -> syn::ItemImpl {
        fold_file(r#impl, self)
    }

    fn fold_item_trait(&mut self, r#trait: syn::ItemTrait) -> syn::ItemTrait {
        fold_file(r#trait, self)
    }
}
