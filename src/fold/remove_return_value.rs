use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveReturnValueVisitor(State);

impl FolderImpl for RemoveReturnValueVisitor {
    const NAME: &'static str = "RemoveReturnValue";

    fn new(state: super::common_folder::State) -> Self {
        RemoveReturnValueVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for RemoveReturnValueVisitor {
    fn fold_expr(&mut self, expr: syn::Expr) -> syn::Expr {
        if self.0.done {
            return expr;
        }

        let span = expr.span();

        if self.0.visited.insert(span.into()) {
            if let syn::Expr::Return(mut ret) = expr {
                ret.expr = Some(syn::parse_quote! { loop { } });
                self.0.done = true;
                return syn::Expr::Return(ret);
            }
        }

        syn::fold::fold_expr(self, expr)
    }

    fn fold_block(&mut self, mut block: syn::Block) -> syn::Block {
        if self.0.done {
            return block;
        }

        let span = block.span();

        if self.0.visited.insert(span.into()) {
            if let Some(syn::Stmt::Expr(expr)) = block.stmts.last_mut() {
                *expr = syn::parse_quote! { loop { } };
                self.0.done = true;
                return block;
            }
        }

        syn::fold::fold_block(self, block)
    }
}
