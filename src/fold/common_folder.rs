use std::marker::PhantomData;

pub struct CommonFolder<T> {
    fold: PhantomData<T>,
    state: State,
}

#[derive(Default)]
pub struct State {
    pub done: bool,
    pub expected_spans: Vec<crate::span::LineColumnSpan>,
    pub visited: std::collections::BTreeSet<crate::span::LineColumnSpan>,
}

impl<T> CommonFolder<T> {
    pub fn new() -> Self {
        Self {
            fold: PhantomData,
            state: State::default(),
        }
    }
}

impl<T: FolderImpl> super::Visiter for CommonFolder<T> {
    fn name(&self) -> &'static str {
        T::NAME
    }

    fn prepare_next_iteration(&mut self, expected_spans: &[crate::message::Message]) {
        self.state.expected_spans.clear();
        self.state
            .expected_spans
            .extend(expected_spans.iter().flat_map(|e| e.spans()));
        self.state.done = false;
    }

    fn reset(&mut self) {
        self.state.done = false;
        self.state.visited.clear();
    }

    fn visited_count(&self) -> usize {
        self.state.visited.len()
    }

    fn made_change(&self) -> bool {
        self.state.done
    }
}

pub trait FolderImpl: syn::fold::Fold + syn::fold::Fold {
    const NAME: &'static str;
    fn new(state: State) -> Self;

    fn deconstruct(self) -> State;
}

impl<T: FolderImpl> syn::fold::Fold for CommonFolder<T> {
    fn fold_file(&mut self, node: syn::File) -> syn::File {
        if self.state.done {
            return node;
        }
        let mut folder_impl = T::new(std::mem::take(&mut self.state));
        let result = folder_impl.fold_file(node);
        self.state = folder_impl.deconstruct();
        result
    }

    fn fold_item_impl(&mut self, node: syn::ItemImpl) -> syn::ItemImpl {
        if self.state.done {
            return node;
        }
        let mut folder_impl = T::new(std::mem::take(&mut self.state));
        let result = folder_impl.fold_item_impl(node);
        self.state = folder_impl.deconstruct();
        result
    }

    fn fold_item_trait(&mut self, node: syn::ItemTrait) -> syn::ItemTrait {
        if self.state.done {
            return node;
        }
        let mut folder_impl = T::new(std::mem::take(&mut self.state));
        let result = folder_impl.fold_item_trait(node);
        self.state = folder_impl.deconstruct();
        result
    }
}
