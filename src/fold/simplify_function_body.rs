use super::common_folder::{FolderImpl, State};
#[derive(Default)]
pub struct SimplifyFunctionBodyVisitor(State);

impl FolderImpl for SimplifyFunctionBodyVisitor {
    const NAME: &'static str = "SimplifyFunctionBody";

    fn new(state: super::common_folder::State) -> Self {
        SimplifyFunctionBodyVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

trait Function {
    fn fold(self, visitor: &mut SimplifyFunctionBodyVisitor) -> Self;

    fn replace_block<F>(&mut self, f: F) -> bool
    where
        F: for<'a> Fn(&'a mut syn::Signature) -> syn::Block;
}

impl Function for syn::ItemFn {
    fn fold(self, visitor: &mut SimplifyFunctionBodyVisitor) -> Self {
        syn::fold::fold_item_fn(visitor, self)
    }

    fn replace_block<F>(&mut self, f: F) -> bool
    where
        F: for<'a> Fn(&'a mut syn::Signature) -> syn::Block,
    {
        self.block = Box::new(f(&mut self.sig));
        true
    }
}

impl Function for syn::ImplItemMethod {
    fn fold(self, visitor: &mut SimplifyFunctionBodyVisitor) -> Self {
        syn::fold::fold_impl_item_method(visitor, self)
    }

    fn replace_block<F>(&mut self, f: F) -> bool
    where
        F: for<'a> Fn(&'a mut syn::Signature) -> syn::Block,
    {
        self.block = f(&mut self.sig);
        true
    }
}

impl Function for syn::TraitItemMethod {
    fn fold(self, visitor: &mut SimplifyFunctionBodyVisitor) -> Self {
        syn::fold::fold_trait_item_method(visitor, self)
    }

    fn replace_block<F>(&mut self, f: F) -> bool
    where
        F: for<'a> Fn(&'a mut syn::Signature) -> syn::Block,
    {
        // we usually can't just remove the default implementation
        // as this would cause more errors in the implementors
        if self.default.is_some() {
            self.default = Some(f(&mut self.sig));
            true
        } else {
            false
        }
    }
}

fn fold<T: Function + syn::spanned::Spanned>(
    visitor: &mut SimplifyFunctionBodyVisitor,
    mut func: T,
) -> T {
    if visitor.0.done {
        return func;
    }

    let item = func.span();

    fn replace(sig: &mut syn::Signature) -> syn::Block {
        #[cfg(feature = "debug")]
        tracing::info!("Replacing `{}`'s body", sig.ident);

        let block = if let syn::ReturnType::Default = sig.output {
            syn::parse_quote! { { } }
        } else {
            syn::parse_quote! { { loop {} } }
        };

        for input in sig.inputs.iter_mut() {
            if let syn::FnArg::Typed(input) = input {
                *input.pat = syn::parse_quote! { _ };
            }
        }

        block
    }

    if visitor.0.visited.insert(item.into())
        && !visitor
            .0
            .expected_spans
            .iter()
            .any(|s| s.intersects(&item.into()))
        && func.replace_block(replace)
    {
        visitor.0.done = true;
        func
    } else {
        func.fold(visitor)
    }
}

impl syn::fold::Fold for SimplifyFunctionBodyVisitor {
    fn fold_item_fn(&mut self, func: syn::ItemFn) -> syn::ItemFn {
        fold(self, func)
    }

    fn fold_impl_item_method(&mut self, func: syn::ImplItemMethod) -> syn::ImplItemMethod {
        fold(self, func)
    }

    fn fold_trait_item_method(&mut self, func: syn::TraitItemMethod) -> syn::TraitItemMethod {
        fold(self, func)
    }
}
