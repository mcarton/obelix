pub mod common_folder;

mod remove_enum_variant;
mod remove_item_impl;
mod remove_match_arms;
mod remove_return_value;
mod remove_statement;
mod remove_struct_field;
mod simplify_function_body;
mod simplify_if;
pub use remove_enum_variant::RemoveEnumVariantVisitor;
pub use remove_item_impl::RemoveItemImplVisitor;
pub use remove_match_arms::RemoveMatchArms;
pub use remove_return_value::RemoveReturnValueVisitor;
pub use remove_statement::RemoveStatementVisitor;
pub use remove_struct_field::RemoveStructFieldVisitor;
pub use simplify_function_body::SimplifyFunctionBodyVisitor;
pub use simplify_if::SimplifyIfVisitor;

#[cfg(feature = "debug")]
macro_rules! enter_span {
    ($t:expr, $name:expr) => {
        let root = tracing::span!(
            tracing::Level::TRACE,
            $name,
            visitor = $t.name(),
            error = false,
            same_errors = tracing::field::Empty,
            same_files = tracing::field::Empty,
        );
        let _enter = root.enter();
    };
}

#[cfg(not(feature = "debug"))]
macro_rules! enter_span {
    ($($t:tt)*) => {};
}

pub trait Visiter: syn::fold::Fold {
    fn name(&self) -> &'static str;

    fn prepare_next_iteration(&mut self, expected_errors: &[crate::message::Message]);
    fn reset(&mut self);

    fn visited_count(&self) -> usize;

    fn made_change(&self) -> bool;
}

#[must_use]
pub(crate) fn fold(
    compiler: &crate::Compiler,
    visitor: &mut dyn Visiter,
    expected_errors: &[crate::message::Message],
    content: &mut syn::File,
) -> bool {
    enter_span!(visitor, "fold");

    let mut made_change = false;
    visitor.reset();

    let mut errors;
    let mut visitor_errors = expected_errors;
    let mut previous_count = visitor.visited_count();
    loop {
        enter_span!(visitor, "fold looping");

        visitor.prepare_next_iteration(visitor_errors);
        let mut new_file = {
            enter_span!(visitor, "sub-fold");
            visitor.fold_file(content.clone())
        };
        let new_count = visitor.visited_count();

        if !visitor.made_change() {
            break made_change;
        }

        let (spans_invalidated, new_errors) = compiler.compile(&mut new_file);

        let same_errors = expected_errors
            .iter()
            .filter(|m| !m.is_dead_code() && !m.ignorable_for_equality())
            .eq(new_errors
                .as_slice()
                .iter()
                .filter(|m| !m.is_dead_code() && !m.ignorable_for_equality()));

        #[cfg(feature = "debug")]
        {
            for (i, (e, n)) in expected_errors.iter().zip(new_errors.iter()).enumerate() {
                if e != n {
                    tracing::info!("Error {}:\n{:?}\n{:?}", i, e, n);
                }
            }
            tracing::Span::current().record("same_errors", &same_errors);
            tracing::Span::current().record("same_files", &(new_file == *content));
        }

        if same_errors && !expected_errors.is_empty() && new_file != *content {
            *content = new_file;
            made_change = true;

            errors = new_errors;
            visitor_errors = &errors;
            if spans_invalidated {
                visitor.reset();
            }

            #[cfg(feature = "debug")]
            tracing::Span::current().record("error", &true);

            #[cfg(feature = "debug")]
            tracing::event!(tracing::Level::INFO, "made change");
        } else if previous_count == new_count {
            break made_change;
        }

        previous_count = new_count;
    }
}
