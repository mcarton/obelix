use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveMatchArms(State);

impl FolderImpl for RemoveMatchArms {
    const NAME: &'static str = "RemoveMatchArms";

    fn new(state: super::common_folder::State) -> Self {
        RemoveMatchArms(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for RemoveMatchArms {
    fn fold_expr(&mut self, mut expr: syn::Expr) -> syn::Expr {
        if self.0.done {
            return expr;
        }

        let span = expr.span();

        if self.0.visited.insert(span.into()) {
            if let syn::Expr::Match(mexpr) = expr {
                if let Some(arm) = single_arm(&mexpr) {
                    let pat = &arm.pat;
                    let iexpr = mexpr.expr.as_ref();
                    let body = make_block(&arm.body);
                    expr = syn::parse_quote! {
                        if let #pat = #iexpr #body
                    };

                    self.0.done = true;
                    expr
                } else {
                    syn::fold::fold_expr(self, syn::Expr::Match(mexpr))
                }
            } else {
                syn::fold::fold_expr(self, expr)
            }
        } else {
            syn::fold::fold_expr(self, expr)
        }
    }

    fn fold_expr_match(&mut self, mut expr: syn::ExprMatch) -> syn::ExprMatch {
        if self.0.done {
            return expr;
        }

        let mut i = expr.arms.len();

        while i != 0 {
            i = i.saturating_sub(1);

            let arm = expr.arms[i].span();

            if self.0.visited.insert(arm.into()) {
                expr.arms.remove(i).span();
                self.0.done = true;
                return expr;
            }

            let pattern = expr.arms[i].pat.span();

            if self.0.visited.insert(pattern.into()) {
                expr.arms[i].pat = syn::parse_quote! { _ };
                self.0.done = true;
                return expr;
            }
        }

        syn::fold::fold_expr_match(self, expr)
    }
}

fn single_arm(expr: &syn::ExprMatch) -> Option<&syn::Arm> {
    match expr.arms.len() {
        1 => Some(&expr.arms[0]),
        2 => {
            if is_match_all_do_nothing(&expr.arms[0]) {
                Some(&expr.arms[1])
            } else if is_match_all_do_nothing(&expr.arms[1]) {
                Some(&expr.arms[0])
            } else {
                None
            }
        }
        _ => None,
    }
}

fn is_match_all_do_nothing(arm: &syn::Arm) -> bool {
    if let syn::Pat::Wild(_) = arm.pat {
        is_do_nothing(&arm.body)
    } else {
        false
    }
}

fn is_do_nothing(expr: &syn::Expr) -> bool {
    match expr {
        syn::Expr::Tuple(tup) if tup.elems.is_empty() => true,
        syn::Expr::Block(block) => block.block.stmts.is_empty(),
        _ => false,
    }
}

fn make_block(expr: &syn::Expr) -> syn::ExprBlock {
    if let syn::Expr::Block(block) = expr {
        block.clone()
    } else {
        syn::parse_quote! {{
            #expr
        }}
    }
}
