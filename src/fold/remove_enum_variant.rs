use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveEnumVariantVisitor(State);

impl FolderImpl for RemoveEnumVariantVisitor {
    const NAME: &'static str = "RemoveEnumVariant";

    fn new(state: super::common_folder::State) -> Self {
        RemoveEnumVariantVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for RemoveEnumVariantVisitor {
    fn fold_item_enum(&mut self, mut e: syn::ItemEnum) -> syn::ItemEnum {
        let mut i = e.variants.len();

        while i != 0 {
            i = i.saturating_sub(1);
            let stmt = e.variants[i].span();

            if self.0.visited.insert(stmt.into()) {
                self.0.done = true;
                e.variants = e
                    .variants
                    .into_iter()
                    .enumerate()
                    .filter(|(n, _)| *n != i)
                    .map(|(_, p)| p)
                    .collect();
                return e;
            }
        }

        e
    }
}
