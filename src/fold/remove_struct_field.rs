use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveStructFieldVisitor(State);

impl FolderImpl for RemoveStructFieldVisitor {
    const NAME: &'static str = "RemoveStructField";

    fn new(state: super::common_folder::State) -> Self {
        RemoveStructFieldVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for RemoveStructFieldVisitor {
    fn fold_fields(&mut self, f: syn::Fields) -> syn::Fields {
        if self.0.done {
            return f;
        }

        if let syn::Fields::Named(mut named) = f {
            if named.named.is_empty() {
                let span = named.named.span();

                if self.0.visited.insert(span.into()) {
                    self.0.done = true;
                    syn::Fields::Unit
                } else {
                    syn::Fields::Named(named)
                }
            } else {
                let mut i = named.named.len();

                while i != 0 {
                    i = i.saturating_sub(1);
                    let stmt = named.named[i].span();

                    if self.0.visited.insert(stmt.into()) {
                        self.0.done = true;
                        let trailing = named.named.trailing_punct();
                        named.named = named
                            .named
                            .into_iter()
                            .enumerate()
                            .filter(|(n, _)| *n != i)
                            .map(|(_, p)| p)
                            .collect();

                        if trailing && !named.named.is_empty() {
                            named.named.push_punct(Default::default());
                        }
                        return syn::Fields::Named(named);
                    }
                }

                syn::Fields::Named(named)
            }
        } else {
            f
        }
    }
}
