use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct SimplifyIfVisitor(State);

impl FolderImpl for SimplifyIfVisitor {
    const NAME: &'static str = "SimplifyIf";

    fn new(state: super::common_folder::State) -> Self {
        SimplifyIfVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for SimplifyIfVisitor {
    fn fold_block(&mut self, mut block: syn::Block) -> syn::Block {
        if self.0.done {
            return block;
        }

        let mut i = block.stmts.len();

        while i != 0 {
            i = i.saturating_sub(1);
            let stmt = block.stmts[i].span();

            if let syn::Stmt::Expr(syn::Expr::If(_)) = &block.stmts[i] {
                if let syn::Stmt::Expr(syn::Expr::If(expr)) = block.stmts.remove(i) {
                    if self.0.visited.insert(stmt.into()) {
                        let span = expr.cond.span();
                        block.stmts.splice(
                            i..i,
                            std::iter::once(syn::Stmt::Semi(*expr.cond, syn::Token![;](span)))
                                .chain(make_semi(expr.then_branch).stmts)
                                .chain(expr.else_branch.into_iter().flat_map(|(_, expr)| {
                                    std::iter::once(syn::Stmt::Semi(*expr, syn::Token![;](span)))
                                })),
                        );
                        self.0.done = true;
                        return block;
                    }
                }
            }
        }

        syn::fold::fold_block(self, block)
    }

    fn fold_expr(&mut self, expr: syn::Expr) -> syn::Expr {
        if self.0.done {
            return expr;
        }

        if let syn::Expr::If(expr) = expr {
            let span = expr.span();

            if self.0.visited.insert(span.into()) {
                self.0.done = true;
                let cond = expr.cond;
                let then_branch = expr.then_branch;
                let else_branch = expr.else_branch.map(|(_, e)| e);
                syn::parse_quote! {{
                    #cond;
                    #then_branch
                    #else_branch
                }}
            } else {
                syn::Expr::If(syn::fold::fold_expr_if(self, expr))
            }
        } else {
            syn::fold::fold_expr(self, expr)
        }
    }
}

fn make_semi(mut block: syn::Block) -> syn::Block {
    if let Some(mut last) = block.stmts.pop() {
        if let syn::Stmt::Expr(expr) = last {
            last = if !is_blocked_expr(&expr) {
                let span = expr.span();
                syn::Stmt::Semi(expr, syn::Token![;](span))
            } else {
                syn::Stmt::Expr(expr)
            }
        }
        block.stmts.push(last);
    }

    block
}

fn is_blocked_expr(expr: &syn::Expr) -> bool {
    match expr {
        syn::Expr::Async(_)
        | syn::Expr::Block(_)
        | syn::Expr::ForLoop(_)
        | syn::Expr::If(_)
        | syn::Expr::Loop(_)
        | syn::Expr::Match(_)
        | syn::Expr::TryBlock(_)
        | syn::Expr::While(_) => true,

        _ => false,
    }
}
