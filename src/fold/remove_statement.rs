use super::common_folder::{FolderImpl, State};
use syn::spanned::Spanned;

#[derive(Default)]
pub struct RemoveStatementVisitor(State);

impl FolderImpl for RemoveStatementVisitor {
    const NAME: &'static str = "RemoveStatement";

    fn new(state: super::common_folder::State) -> Self {
        RemoveStatementVisitor(state)
    }

    fn deconstruct(self) -> super::common_folder::State {
        self.0
    }
}

impl syn::fold::Fold for RemoveStatementVisitor {
    fn fold_block(&mut self, mut block: syn::Block) -> syn::Block {
        if self.0.done {
            return block;
        }

        let mut i = block.stmts.len();

        if i == 0 {
            return block;
        }

        while i != 0 {
            i = i.saturating_sub(1);
            let stmt = block.stmts[i].span();

            if self.0.visited.insert(stmt.into())
                && !self
                    .0
                    .expected_spans
                    .iter()
                    .any(|s| s.intersects(&stmt.into()))
            {
                block.stmts.remove(i);
                self.0.done = true;
                return block;
            }
        }

        syn::fold::fold_block(self, block)
    }
}
