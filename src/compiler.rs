use quote::ToTokens;

use crate::message::Message;

#[derive(Debug, Default)]
pub struct Compiler {
    pub toolchain: Option<String>,
}

impl Compiler {
    /// Compile a file, potentially updating it to remove unused code and returns
    /// whether any change has been made that would invalidate cached spans.
    #[cfg_attr(feature = "debug", tracing_attributes::instrument)]
    pub fn compile(&self, content: &mut syn::File) -> (bool, Vec<Message>) {
        let mut child = std::process::Command::new("rustc")
            .args(&[
                "--error-format=json",
                "-",
                "--out-dir",
                "/tmp",
                "--emit",
                "metadata",
                "--crate-type",
                "lib",
            ])
            .envs(self.toolchain.as_ref().map(|v| ("RUSTUP_TOOLCHAIN", v)))
            .stdin(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .spawn()
            .expect("Failed to spawn rustc process");

        let mut content_str = content.to_token_stream().to_string();

        #[cfg(feature = "debug")]
        tracing::Span::current().record("content", &content_str.as_str());

        {
            use std::io::Write;

            let stdin = child.stdin.as_mut().expect("Failed to open rustc stdin");
            stdin
                .write_all(content_str.as_bytes())
                .expect("Failed to write to stdin");
        }

        let output = child
            .wait_with_output()
            .expect("Failed to read rustc output")
            .stderr;

        let messages = Message::from_compiler_output(&output);
        let invalidate_spans = Message::cleanup_unused(&messages, &mut content_str);
        *content =
            syn::parse_str::<syn::File>(&content_str).expect("Could not parse cleaned-up file");

        (invalidate_spans, messages)
    }
}
