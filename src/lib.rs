use fold::common_folder::CommonFolder;
use quote::ToTokens;

mod compiler;
mod fold;
mod message;
mod span;

pub use compiler::Compiler;

pub struct FoldRegistry {
    folders: Vec<Box<dyn fold::Visiter>>,
}

impl FoldRegistry {
    pub fn new() -> FoldRegistry {
        Self {
            folders: vec![
                Box::new(CommonFolder::<fold::SimplifyFunctionBodyVisitor>::new()),
                Box::new(CommonFolder::<fold::RemoveItemImplVisitor>::new()),
                Box::new(CommonFolder::<fold::RemoveReturnValueVisitor>::new()),
                Box::new(CommonFolder::<fold::SimplifyIfVisitor>::new()),
                Box::new(CommonFolder::<fold::RemoveStatementVisitor>::new()),
                Box::new(CommonFolder::<fold::RemoveMatchArms>::new()),
                Box::new(CommonFolder::<fold::RemoveEnumVariantVisitor>::new()),
                Box::new(CommonFolder::<fold::RemoveStructFieldVisitor>::new()),
            ],
        }
    }

    pub fn new_with_passes<'a, T, U>(passes: Option<&'a T>) -> FoldRegistry
    where
        &'a T: IntoIterator<Item = U>,
        U: AsRef<str>,
    {
        let mut registry = Self::new();
        if let Some(passes) = passes {
            registry
                .folders
                .retain(|p| passes.into_iter().any(|n| n.as_ref() == p.name()));
        }
        registry
    }

    pub fn simplify_file(&mut self, compiler: &Compiler, content: &mut syn::File) {
        // make sure we can compare spans:
        *content = syn::parse_file(&content.into_token_stream().to_string()).unwrap();

        let (_, expected_errors) = compiler.compile(content);
        loop {
            let made_change = self.folders.iter_mut().fold(false, |acc, folder| {
                fold::fold(compiler, &mut **folder, &expected_errors, content) || acc
            });

            if !made_change {
                return;
            }
        }
    }
}
