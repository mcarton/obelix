# Obélix

Obélix is a tool to reduce Rust source files to produce MVEs.

## Why use this tool?

Minimization is often useful to understand compiler errors. Isolating the errors and removing the noise is important and can help figuring out the problem. It's also important when asking questions on sites such as Stack Overflow or The Rust Programming
Language Forum. This tools helps automating this process.

The intended usage of this tool is to help users produces minified source code to

- Help them understand why their code doesn't compile.
- Help them produce an MVE to ask a question on Stack Overflow, The Rust Programming Language Forum.
- Help them produce an MVE to open an issue on GitHub for compiler bugs.

Note that minimization isn't always perfect: When asking a question, having some context is sometimes useful. But Obélix can't know whether code is useless or intended to provide context. This means that there often would be a post-processing phase after using this tool.

## How to use it

At the moment, Obélix has a very simple interface.

You can install it using `cargo install obelix`. Just call `obelix your-file.rs` and it will output a simpler producing the same errors as the original file, which you'll probably want to pipe into `rustfmt`:

```
$ cat a.rs 
fn main() {
    error
}

fn unused() {}
$ rustc a.rs 
error[E0425]: cannot find value `error` in this scope
 --> a.rs:2:5
  |
2 |     error
  |     ^^^^^ not found in this scope

error: aborting due to previous error

For more information about this error, try `rustc --explain E0425`.
$ obelix a.rs | rustfmt 
fn main() {
    error
}
$ obelix a.rs | rustc -
error[E0425]: cannot find value `error` in this scope
 --> <anon>:1:15
  |
1 | fn main ( ) { error }
  |               ^^^^^ not found in this scope

error: aborting due to previous error

For more information about this error, try `rustc --explain E0425`.
```

Note that Obélix never overwrites your files.

## Why “Obélix”?

I wanted to give this tool a name that would sound like "mini rust", or "mini-rs" or something. I eventually settled on [menhir], which seemed like a good name especially with _hir_ meaning _long_. Unfortunately the name was already reserved on crates.io. I therefore changed the name to [Obélix](), a menhir sculptor with a slight _weight problem_.

## License

Licensed under either of <a href="LICENSE-APACHE">Apache License, Version
2.0</a> or <a href="LICENSE-MIT">MIT license</a> at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in Obélix by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

All test files named `so-ID` are user contributions from Stack Overflow and
are licenced under [CC BY-SA](https://stackoverflow.com/help/licensing). The
original post is accessible at <https://stackoverflow.com/questions/$ID>.

All test files named `URLO-ID` are user contributions from The Rust Programming
Language Forum and are licensed under
[CC BY-NC-SA 3.0](https://users.rust-lang.org/tos#3). The original post is
accessible at <https://users.rust-lang.org/t/$ID>.

Menhir icon made by Good Ware from
[www.flaticon.com](https://www.flaticon.com/free-icon/menhir_2614647).

[menhir]: https://en.wikipedia.org/wiki/Menhir