trait T<'a> {
    fn insert(&mut self, param: &'a u8) -> ();
}
struct Noop;
impl<'a> T<'a> for Noop {
    fn insert(&mut self, _: &'a u8) {}
}
struct Storage<'a> {
    items: Vec<&'a u8>,
}
impl<'a> Storage<'a> {
    fn new(_: Box<dyn T<'a>>) -> Self {
        loop {}
    }
}
impl<'a> T<'a> for Storage<'a> {
    fn insert(&mut self, _: &'a u8) {}
}
fn foo<'x>(param: &'x u8) {
    let mut two_layers: Storage<'x> =
        Storage::<'x>::new(Box::new(Storage::<'x>::new(Box::new(Noop {}))));
}
