#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ListNode {
    pub val: i32,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { val }
    }
}

fn is_same(a: &Option<ListNode>, b: &Option<ListNode>) -> bool {
    if a == None && b == None {
        return true;
    } else if a == None && b != None {
        return false;
    } else if a != None && b == None {
        return false;
    }

    let ca: ListNode = a.clone().unwrap_or(ListNode::new(0));
    let cb: ListNode = b.clone().unwrap_or(ListNode::new(0));
    if ca.val == cb.val {
        return true;
    } else {
        return false;
    }
}

fn main() {
    let a: Option<ListNode> = Some(ListNode::new(0));
    let b: Option<ListNode> = Some(ListNode::new(0));

    println!("{:?}", is_same(&a, &b));
}
