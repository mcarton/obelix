pub fn create_ars_tree() {
    let mut top_level_node = NodeItem {
        key: StringOrNodeItems::Text(String::new()),
        parameter: StringOrNodeItems::Text(String::new()),
        parent_item: None,
    };
    let mut current_node = &mut top_level_node;
    let current_part = &current_node.key;
    for _current_char in "abcdef".chars() {
        if let StringOrNodeItems::Text(text) = current_part {
            current_node.key = StringOrNodeItems::Keys(vec![NodeItem {
                key: StringOrNodeItems::Text(text.to_string()),
                parameter: StringOrNodeItems::Text(String::new()),
                parent_item: Some(current_node),
            }]);
        }
    }
}
#[derive(Debug)]
pub struct NodeItem<'a> {
    key: StringOrNodeItems<'a>,
    parameter: StringOrNodeItems<'a>,
    parent_item: Option<&'a NodeItem<'a>>,
}
#[derive(Debug)]
enum StringOrNodeItems<'a> {
    Text(String),
    Keys(Vec<NodeItem<'a>>),
}
