use std::sync::{mpsc, Arc, Mutex};
use std::thread;
struct Job;
struct Worker {
    thread: Option<thread::JoinHandle<()>>,
    receiver: Arc<Mutex<mpsc::Receiver<Job>>>,
}
impl Worker {
    fn start(&mut self) {
        self.thread = Some(thread::spawn(move || loop {
            let job = self.receiver.lock().unwrap().recv().unwrap();
        }));
    }
}
