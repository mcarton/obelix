#[derive(Debug)]
struct A {
    option: Option<i32>,
}
impl A {
    fn option_or_new(&mut self) -> &i32 {
        if let Some(i) = &self.option {
            &i
        } else {
            let i = 0;
            self.option = Some(i);
            self.option.as_ref().unwrap()
        }
    }
}
