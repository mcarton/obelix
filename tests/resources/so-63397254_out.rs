use std::borrow::Cow;
pub struct Token<'tok> {
    pub value: Cow<'tok, str>,
}
pub struct Scanner;
impl Scanner {
    pub fn next(&mut self) -> Token<'static> {
        loop {}
    }
}
pub struct Parser<'lexer> {
    pub curr: &'lexer Token<'lexer>,
    scanner: &'lexer mut Scanner,
}
impl<'lexer> Parser<'lexer> {
    pub fn advance(&mut self) {
        self.curr = &self.scanner.next();
    }
}
