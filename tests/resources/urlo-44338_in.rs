use std::fmt::Debug;

pub struct SimpleLinkedList<T: Debug>(Option<Box<Node<T>>>);

struct Node<T: Debug> {
    data: T,
    next: Option<Box<Node<T>>>,
}

impl<T> SimpleLinkedList<T>
where
    T: Debug,
{
    pub fn new() -> Self {
        SimpleLinkedList(None)
    }

    pub fn len(&self) -> usize {
        unimplemented!()
    }

    pub fn push(&mut self, element: T) {
        let first_node = &mut self.0;
        match first_node {
            None => {
                let new_node = Box::new(Node {
                    data: element,
                    next: None,
                });
                println!("This is the beginning");
                *first_node = Some(new_node);
            }
            Some(node) => {
                // let last_node = &mut node.next;

                let new_node = Box::new(Node {
                    data: element,
                    next: Some(*node),
                });
                *first_node = Some(new_node);
            }
        }
    }

    pub fn pop(&mut self) -> Option<T> {
        unimplemented!()
    }

    pub fn peek(&self) -> Option<&T> {
        unimplemented!()
    }

    pub fn rev(self) -> SimpleLinkedList<T> {
        unimplemented!()
    }

    pub fn printlist(&self) {
        let mut last_node = &self.0;
        loop {
            match last_node {
                Some(node) => {
                    println!("Value stored is {:?}", (*node).data);
                    last_node = &(node).next;
                }
                None => break,
            }
        }
    }
}
