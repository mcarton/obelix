use std::borrow::Cow;
pub struct Token<'tok> {
    pub value: Cow<'tok, str>,
    pub line: usize,
}
pub struct Scanner {
    pub source: Vec<char>,
    pub current: usize,
    pub line: usize,
}
impl Scanner {
    pub fn next(&mut self) -> Token<'static> {
        loop {}
    }
}
pub struct Parser<'lexer> {
    pub curr: &'lexer Token<'lexer>,
    pub prev: &'lexer Token<'lexer>,
    scanner: &'lexer mut Scanner,
}
impl<'lexer> Parser<'lexer> {
    pub fn advance(&mut self) {
        self.curr = &self.scanner.next();
    }
}
