use std::sync::{mpsc, Arc, Mutex};
use std::thread;
struct Job {
    x: usize,
}
struct WorkerPool {
    sender: mpsc::Sender<Job>,
    workers: Vec<Worker>,
}
impl WorkerPool {
    fn new(num_workers: usize) -> WorkerPool {
        let mut workers = Vec::with_capacity(num_workers);
        let (sender, receiver) = mpsc::channel();
        let receiver = Arc::new(Mutex::new(receiver));
        for id in 0..num_workers {
            workers.push(Worker::new(id, receiver.clone()));
        }
        WorkerPool { sender, workers }
    }
}
struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
    receiver: Arc<Mutex<mpsc::Receiver<Job>>>,
}
impl Worker {
    fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Job>>>) -> Worker {
        Worker {
            id,
            thread: None,
            receiver,
        }
    }
    fn start(&mut self) {
        self.thread = Some(thread::spawn(move || loop {
            let job = self.receiver.lock().unwrap().recv().unwrap();
            self.add_to_id(job.x);
        }));
    }
    pub fn add_to_id(&self, x: usize) {
        println!("The result is: {}", self.id + x);
    }
}
