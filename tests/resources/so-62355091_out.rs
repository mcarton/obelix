use std::collections::HashMap;
trait Trait<'a> {
    fn get_enum(&'a self) -> Enum<'a>;
}
#[derive(Clone)]
enum Enum<'a> {
    Map(HashMap<String, &'a dyn Trait<'a>>)
}
fn process<'a>(val: &'a dyn Trait<'a>) -> Vec<&'a dyn Trait<'a>> {
    let mut traits: Vec<&dyn Trait> = vec![];
    let Enum::Map(m) = val.get_enum();
    for elem in m.values() {
        traits.push(elem);
    }
    loop {}
}
