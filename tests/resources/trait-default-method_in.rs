trait Foo {
    fn foo() -> bool {
        true
    }
}

impl Foo for String {}

fn main() {
    let _: String = String::foo();
}