trait T<'a> {
    fn insert(&mut self, param: &'a u8) -> ();
}
struct Noop {}
impl<'a> T<'a> for Noop {
    fn insert(&mut self, param: &'a u8) {}
}
struct Storage<'a> {
    items: Vec<&'a u8>,
    parent: Box<dyn T<'a>>,
}
impl<'a> Storage<'a> {
    fn new(parent: Box<dyn T<'a>>) -> Self {
        Storage {
            items: vec![],
            parent,
        }
    }
}
impl<'a> T<'a> for Storage<'a> {
    fn insert(&mut self, param: &'a u8) {
        self.items.push(param);
        self.parent.insert(param);
    }
}
fn foo<'x>(param: &'x u8) {
    let mut one_layer: Storage<'x> = Storage::<'x>::new(Box::new(Noop {}));
    one_layer.insert(param);
    let mut two_layers: Storage<'x> =
        Storage::<'x>::new(Box::new(Storage::<'x>::new(Box::new(Noop {}))));
    two_layers.insert(param);
}
