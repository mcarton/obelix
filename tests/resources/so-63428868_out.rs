struct Node {
    key: i32,
    left: Option<Box<Node>>,
}
struct BinaryTree {
    root: Option<Box<Node>>,
}
impl BinaryTree {
    fn find_mut(&mut self, key: i32) -> &mut Option<Box<Node>> {
        let mut node = &mut self.root;
        loop {
            match node {
                Some(box_node) if box_node.key != key => {
                    node = if box_node.key < key {
                        loop {}
                    } else {
                        &mut box_node.left
                    }
                }
                other => return other,
            }
        }
    }
}
