struct Foo;

impl Foo {
    fn foo(&self, _: u32) -> u32 {
        loop {}
    }
}

fn main() {
    Foo.foo("");
}