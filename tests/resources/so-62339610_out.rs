struct AA;
impl AA {
    pub fn world(self) -> Self {
        loop {}
    }
}
fn hello(aa: &mut AA) {
    aa.world();
}
