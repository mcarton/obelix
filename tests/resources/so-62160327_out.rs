use std::borrow::Borrow;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::RangeBounds;
pub trait TestLabel: Debug + Clone + Sized + PartialEq + Eq + Hash + PartialOrd + Ord {}
impl TestLabel for String {}
pub struct TestIndex<L: TestLabel>(Vec<L>);
impl<L: TestLabel> TestIndex<L> {
    pub fn loc_range<'a, R, Q: 'a>(&self, _: R) -> Option<Vec<usize>>
    where
        R: RangeBounds<&'a Q>,
        L: Borrow<Q>,
        Q: Hash + Eq + ?Sized,
    {
        loop {}
    }
    pub fn test() {
        let i = TestIndex(vec![
            String::from("a"),
            String::from("b"),
            String::from("c"),
        ]);
        println!("{:?}", i.loc_range(..));
    }
}
