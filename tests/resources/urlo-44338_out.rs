use std::fmt::Debug;
pub struct SimpleLinkedList<T: Debug>(Option<Box<Node<T>>>);
struct Node<T: Debug> {
    data: T,
    next: Option<Box<Node<T>>>,
}
impl<T> SimpleLinkedList<T>
where
    T: Debug,
{
    pub fn push(&mut self, element: T) {
        let first_node = &mut self.0;
        match first_node {
            None => {}
            Some(node) => {
                let _new_node = Box::new(Node {
                    data: element,
                    next: Some(*node),
                });
            }
        }
    }
}
