use std::borrow::Borrow;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::RangeBounds;
pub trait TestLabel: Debug + Clone + Sized + PartialEq + Eq + Hash + PartialOrd + Ord {}
impl TestLabel for char {}
impl TestLabel for String {}
pub struct TestIndex<L: TestLabel>(Vec<L>);
impl<L: TestLabel> TestIndex<L> {
    pub fn loc_range<'a, R, Q: 'a>(&self, range: R) -> Option<Vec<usize>>
    where
        R: RangeBounds<&'a Q>,
        L: Borrow<Q>,
        Q: Hash + Eq + ?Sized,
    {
        // Actual code uses `IndexSet` and does a set lookup,
        // hence the use of `Borrow`.
        // This is just dummy code to get a minimum working example.
        None
    }
    pub fn test() {
        let i = TestIndex(vec!['a', 'b', 'c']);

        // OK!
        println!("{:?}", i.loc_range(&'a'..&'c'));
        println!("{:?}", i.loc_range(..&'c'));
        println!("{:?}", i.loc_range(&'a'..));
        println!("{:?}", i.loc_range(..));

        let i = TestIndex(vec![
            String::from("a"),
            String::from("b"),
            String::from("c"),
        ]);

        // Strange that no '&' is needed, but compiles
        println!("{:?}", i.loc_range("a".."c"));
        println!("{:?}", i.loc_range(.."c"));
        println!("{:?}", i.loc_range("a"..));

        // ERROR E0283
        // cannot resolve `std::string::String: std::borrow::Borrow<_>`
        println!("{:?}", i.loc_range(..));

        // Compiles, but feels very unergonomic!
        println!("{:?}", i.loc_range::<_, str>(..));
    }
}