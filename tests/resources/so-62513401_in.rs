struct Point<T>
where
    T: std::ops::Add<Output = T>,
{
    x: T,
    y: T,
}

impl Point<f32> {
    fn add_f32(&self) -> f32 {
        self.x + self.y
    }
}

//error
impl<T> Point<T> {
    fn add(&self) -> T
    where
        T: std::ops::Add<Output = T>,
    {
        self.x + self.y
    }
}
