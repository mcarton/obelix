struct Foo;

impl Foo {
    fn foo(&self, param: u32) -> u32 {
        param
    }
}

fn main() {
    Foo.foo("");
}