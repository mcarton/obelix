// obelix: passes: SimplifyFunctionBody

fn main() {
    foo()
}

fn foo() -> i32 {
    42
}
