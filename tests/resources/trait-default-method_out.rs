trait Foo {
    fn foo() -> bool {
        loop {}
    }
}
impl Foo for String {}
fn main() {
    let _: String = String::foo();
}