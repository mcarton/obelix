#[derive(Debug)]
struct A {
    option: Option<i32>,
}
impl A {
    fn new() -> Self {
        Self { option: None }
    }
    fn option_or_new(&mut self) -> &i32 {
        if let Some(i) = &self.option { // `self.option` is borrowed here
            &i
        } else {
            let i = 0;
            self.option = Some(i); // cannot assign to `self.option` because it is borrowed
            self.option.as_ref().unwrap()
        }
    }
}

fn main() {
    let mut a = A::new();
    let i = a.option_or_new();
    println!("{}",i);
}