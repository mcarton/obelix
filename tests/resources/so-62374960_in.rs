use std::collections::*;
use std::sync::{Arc, RwLock};

fn main() {
    let mut items: HashMap<i32, Arc<RwLock<dyn Trait>>> = HashMap::new();

    for item in items.values_mut() {
        item.write().unwrap().foo_mut();
    }

    ()
}