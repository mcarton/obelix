struct AA {
    size: i8,
}

impl AA {
    pub fn create() -> Self {
        Self { size: 10 }
    }
    pub fn world(mut self) -> Self {
        self.size += 2;
        self
    }
    pub fn say(self) {
        println!("{}", self.size);
    }
}

fn hello(aa: &mut AA) {
    aa.world();
}

fn main() {
    let mut a = AA::create();
    hello(&mut a);
    a.say();
}
