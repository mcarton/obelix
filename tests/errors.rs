use quote::ToTokens;
use std::io::Read;

#[derive(Debug, Default)]
struct TestConfig {
    rustc_version: Option<String>,
    /// The list of passes that are to be used, none if all passes must be used.
    passes: Option<Vec<String>>,
}

fn main() {
    let args: Vec<_> = std::env::args().collect();
    init_tracer().unwrap();
    rustc_test::test_main(&args, tests("./tests/resources"));
}

#[test]
#[ignore]
/// Convenient way to run a single test
fn error() {
    init_tracer().unwrap();

    if let Err(e) = test_one(
        &"./tests/resources/ghrlr-73730_in.rs",
        "ghrlr-73730",
        &TestConfig::default(),
    ) {
        panic!("Error: {:?}", e);
    }
}

#[cfg(not(feature = "debug"))]
fn init_tracer() -> Result<(), Box<dyn std::error::Error>> {
    Ok(())
}

#[cfg(feature = "debug")]
fn init_tracer() -> Result<(), Box<dyn std::error::Error>> {
    use opentelemetry::api::Provider;
    use opentelemetry::sdk;
    use tracing_subscriber::prelude::*;

    let exporter = opentelemetry_jaeger::Exporter::builder()
        .with_agent_endpoint("127.0.0.1:6831".parse().unwrap())
        .with_process(opentelemetry_jaeger::Process {
            service_name: "obelix".to_string(),
            tags: Vec::new(),
        })
        .init()?;
    let provider = sdk::Provider::builder()
        .with_simple_exporter(exporter)
        .with_config(sdk::Config {
            default_sampler: Box::new(sdk::Sampler::Always),
            ..Default::default()
        })
        .build();
    let tracer = provider.get_tracer("tracing");

    let opentelemetry = tracing_opentelemetry::layer().with_tracer(tracer);
    tracing_subscriber::registry()
        .with(opentelemetry)
        .try_init()?;

    Ok(())
}

fn tests<P: AsRef<std::path::Path> + std::fmt::Debug>(path: P) -> Vec<rustc_test::TestDescAndFn> {
    let files = std::fs::read_dir(path).expect("Could not iterate resource folder");

    files
        .map(|file| {
            let file = file.expect("Could not read directory entry");
            file.path()
        })
        .filter_map(|p| filter_path(&p).map(|sp| (p, sp)))
        .map(|(file, base_name)| {
            let test_config = parse_header(&file);
            rustc_test::TestDescAndFn {
                desc: rustc_test::TestDesc {
                    name: rustc_test::TestName::DynTestName(base_name.clone()),
                    ignore: false,
                    should_panic: rustc_test::ShouldPanic::No,
                    allow_fail: false,
                },
                testfn: rustc_test::TestFn::DynTestFn(Box::new(move || {
                    test_one(&file, &base_name, &test_config)
                })),
            }
        })
        .collect::<Vec<_>>()
}

fn filter_path<P: AsRef<std::path::Path> + std::fmt::Debug>(file: P) -> Option<String> {
    let file = file.as_ref();
    let file_name = file
        .file_name()
        .expect("Invalid path")
        .to_str()
        .expect("Invalid path");

    if file_name.starts_with("nightly-") {
        return None;
    }

    let mut file_name = file_name.rsplitn(2, '_');

    let (long_extension, file_name) = (
        file_name.next().expect("Invalid path: no extension"),
        file_name
            .next()
            .unwrap_or_else(|| panic!("Invalid path: no filename: {}", file.display())),
    );

    match long_extension {
        "in.rs" => Some(file_name.to_string()),
        "out.rs" => None,
        "result.rs" => None,
        e => panic!("Invalid extension `{}`", e),
    }
}

fn parse_header<P: AsRef<std::path::Path>>(file: P) -> TestConfig {
    let mut config = TestConfig::default();

    let file = std::fs::read_to_string(file).unwrap();
    let prefix = "// obelix: ";
    let lines = file
        .lines()
        .take_while(|line| line.starts_with(prefix))
        .map(|l| &l[prefix.len()..])
        .collect::<Vec<_>>();

    for line in lines {
        match line {
            _ if line.starts_with("nightly") => config.rustc_version = Some(line.to_string()),
            _ if line.starts_with("passes:") => {
                config.passes = Some(
                    line["passes:".len()..]
                        .split(",")
                        .map(|s| s.trim())
                        .map(str::to_owned)
                        .collect(),
                )
            }
            _ => panic!("Invalid test configuration"),
        }
    }

    config
}

#[cfg_attr(feature = "debug", tracing_attributes::instrument)]
fn test_one<P: AsRef<std::path::Path> + std::fmt::Debug>(
    file: P,
    base_name: &str,
    test_config: &TestConfig,
) {
    let file = file.as_ref();

    let expected = {
        let mut expected = String::new();
        let mut output_file =
            std::fs::File::open(file.with_file_name(format!("{}_out.rs", base_name)))
                .expect("Could not read file");
        output_file
            .read_to_string(&mut expected)
            .expect("Could not read file content");

        syn::parse_str::<syn::File>(&expected).expect("Could not parse expected file")
    };

    let buffer = {
        let mut buffer = String::new();
        let mut file = std::fs::File::open(file).expect("Could not read file");
        file.read_to_string(&mut buffer)
            .expect("Could not read file content");

        buffer
    };

    let mut parsed_file = syn::parse_str::<syn::File>(&buffer).expect("Could not parse file");

    let compiler = obelix::Compiler {
        toolchain: test_config.rustc_version.as_ref().map(|v| v.to_string()),
        ..obelix::Compiler::default()
    };

    let mut registry = obelix::FoldRegistry::new_with_passes(test_config.passes.as_ref());
    registry.simplify_file(&compiler, &mut parsed_file);

    if expected != parsed_file {
        use std::io::Write;

        let mut output_file =
            std::fs::File::create(file.with_file_name(format!("{}_result.rs", base_name)))
                .expect("Could not create file");

        output_file
            .write(parsed_file.into_token_stream().to_string().as_bytes())
            .expect("Could not write file");

        drop(output_file);

        panic!()
    }
}
